var gulp = require('gulp');
var pug = require('gulp-pug');
var watch = require('gulp-watch');
var minify = require('gulp-minify');
var less = require('gulp-less');
var minifyCSS = require('gulp-csso');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');
var imagemin = require('gulp-imagemin');
var path = require("path");
var imageResize = require('gulp-image-resize');

  gulp.task('minimg', () =>
    gulp.src('./img/*')
        .pipe(imagemin())
        .pipe(gulp.dest('build/img/'))
  );
  gulp.task('html', function(){
    return gulp.src('./*.pug')
      .pipe(pug())
      .pipe(gulp.dest('./build/'))
      .pipe(livereload());
  });
  gulp.task('css', function(){
    return gulp.src('./css/less/*.less')
      .pipe(less())
      .pipe(minifyCSS())
      // .pipe(sourcemaps.init())
      .pipe(concat('styles.css'))
      // .pipe(sourcemaps.write())
      .pipe(gulp.dest('./build/'))
  });
  
  gulp.task('js', function(){
    return gulp.src('js/*.js')
      .pipe(sourcemaps.init())
      .pipe(concat('main.js'))
      .pipe(minify())
      .pipe(sourcemaps.write())
      .pipe(gulp.dest('./build/'))
  });
  
gulp.task('less', function () {
  return gulp.src('./css/less/*.less')
    .pipe(less({
      paths: [ path.join(__dirname, 'less', 'includes') ]
    }))
    .pipe(gulp.dest('./css'));
});
gulp.task('watch', function() {
  livereload.listen();
  gulp.watch('./css/less/*.less', ['css']);
  gulp.watch('./*.pug', ['html']);
  gulp.watch('./js/*.js', ['js']);
});
gulp.task('resizeimg', function(){
  gulp.src('./img/resize/*')
    .pipe(imageResize({
      width : 350,
      crop : false,
      upscale : false
    }))
    .pipe(gulp.dest('./build/img/capsulas/'));
});
gulp.task('default', [ 'watch', 'minimg']);