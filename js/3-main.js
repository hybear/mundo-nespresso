tns({
    container: '.slider',
    mode: 'carousel', // or 'gallery'
    axis: 'horizontal', // or 'vertical'
    items: 1,
    gutter: 0, 
    edgePadding: 0,
    fixedWidth: false,
    slideBy: 1,
    controls: true,
    controlsText: ['', ''],
    controlsContainer: false,
    nav: true,
    navContainer: false,
    navAsThumbnails: false,
    arrowKeys: false,
    speed: 300,
    autoplay: true,
    autoplayTimeout: 13000,
    autoplayDirection: 'forward',
    autoplayText: ['start', 'stop'],
    autoplayHoverPause: false,
    autoplayButton: false,
    autoplayButtonOutput: false,
    autoplayResetOnVisibility: true,
    loop: true,
    rewind: false,
    autoHeight: false,
    responsive: false,
    lazyload: false,
    touch: true,
    mouseDrag: false,
    swipeAngle: 15,
    nested: false,
    freezable: false,
    onInit: false,
    responsive: {
        400:{
            items: 2
        },
        640: {
            items: 2
        },
        768: {
            items: 2,
            nav: false
        },
        992: {
            items: 3,
            nav: false
        }
    }
  });
tns({
    container: '.slidermaquinas',
    mode: 'carousel', // or 'gallery'
    axis: 'horizontal', // or 'vertical'
    items: 1,
    gutter: 0,
    edgePadding: 0,
    fixedWidth: false,
    slideBy: 1,
    controls: true,
    controlsText: ['', ''],
    controlsContainer: false,
    nav: false,
    navContainer: false,
    navAsThumbnails: false,
    arrowKeys: false,
    speed: 3000,
    autoplay: true,
    autoplayTimeout: 3000,
    autoplayDirection: 'forward',
    autoplayText: ['start', 'stop'],
    autoplayHoverPause: false,
    autoplayButton: false,
    autoplayButtonOutput: false,
    autoplayResetOnVisibility: true,
    loop: true,
    rewind: false,
    autoHeight: false,
    responsive: false,
    lazyload: false,
    touch: true,
    mouseDrag: false,
    swipeAngle: 15,
    nested: false,
    freezable: false,
    onInit: false,
    responsive: {
        768: {
            nav: false
        }
    }
  });
tns({
    container: '.historias',
    mode: 'carousel', // or 'gallery'
    axis: 'horizontal', // or 'vertical'
    items: 1,
    gutter: 0,
    slideBy: 1,
    controls: true,
    controlsText: ['', ''],
    controlsContainer: false,
    nav: false,
    navContainer: false,
    navAsThumbnails: false,
    speed: 300,
    autoplay: false,
    loop: false,
    rewind: false,
    autoHeight: false,
    responsive: false,
    lazyload: false,
    touch: true,
    mouseDrag: true,
    swipeAngle: 15,
    nested: false,
    freezable: false,
    nav: true,
    onInit: false,
    "swipeAngle": false,
    responsive: {
        768: {
            nav: false
        }
    }
  });
  (function(){
    var nav = document.querySelectorAll(".tns-nav"),
    item = document.querySelectorAll(".tns-ovh");

    item[0].insertBefore(nav[0], nav[0].nextElementSibling.nextElementSibling);
    item[1].insertBefore(nav[1], nav[1].nextElementSibling.nextElementSibling);
  })();
// init Isotope
var iso = new Isotope( '.grid',{
  itemSelector: '.capsula-card',
});

// store filter for each group
var filters = {};

var filtersElem = document.querySelector('.filters');
filtersElem.addEventListener( 'click', function( event ) {
  // check for only button clicks
  var isButton = event.target.classList.contains('button');
  var active = event.target.classList.contains('active');
  if ( !isButton ) {
    return;
  }
  // set filter for group
  var buttonGroup = fizzyUIUtils.getParent( event.target, '.button-group' );
  var filterGroup = buttonGroup.getAttribute('data-filter-group');
  var filterValue = "";
  if(!active){
    event.target.parentElement.querySelector(".any").classList.add("active");
    filters[ filterGroup ] = event.target.parentElement.querySelector(".active").getAttribute('data-filter');
    filterValue = concatValues( filters );
    iso.arrange({ filter: filterValue });
    document.getElementById("result").innerHTML = filterValue.split(".").join(" ").replace("t-", " ");
  }else{
    filters[ filterGroup ] = event.target.getAttribute('data-filter');
    // combine filters
    filterValue = concatValues( filters );
    // set filter for Isotope
    iso.arrange({ filter: filterValue });
    filterValue = filterValue.split(".").join(" ");
    document.getElementById("result").innerHTML = filterValue.split(".").join(" ").replace("t-", " ");
  }
});
function eventFire(el, etype){
  if (el.fireEvent) {
    el.fireEvent('on' + etype);
  } else {
    var evObj = document.createEvent('Events');
    evObj.initEvent(etype, true, false);
    el.dispatchEvent(evObj);
  }
}var ready = (function(){

    var readyList,
        DOMContentLoaded,
        class2type = {};
        class2type["[object Boolean]"] = "boolean";
        class2type["[object Number]"] = "number";
        class2type["[object String]"] = "string";
        class2type["[object Function]"] = "function";
        class2type["[object Array]"] = "array";
        class2type["[object Date]"] = "date";
        class2type["[object RegExp]"] = "regexp";
        class2type["[object Object]"] = "object";

    var ReadyObj = {
        // Is the DOM ready to be used? Set to true once it occurs.
        isReady: false,
        // A counter to track how many items to wait for before
        // the ready event fires. See #6781
        readyWait: 1,
        // Hold (or release) the ready event
        holdReady: function( hold ) {
            if ( hold ) {
                ReadyObj.readyWait++;
            } else {
                ReadyObj.ready( true );
            }
        },
        // Handle when the DOM is ready
        ready: function( wait ) {
            // Either a released hold or an DOMready/load event and not yet ready
            if ( (wait === true && !--ReadyObj.readyWait) || (wait !== true && !ReadyObj.isReady) ) {
                // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
                if ( !document.body ) {
                    return setTimeout( ReadyObj.ready, 1 );
                }

                // Remember that the DOM is ready
                ReadyObj.isReady = true;
                // If a normal DOM Ready event fired, decrement, and wait if need be
                if ( wait !== true && --ReadyObj.readyWait > 0 ) {
                    return;
                }
                // If there are functions bound, to execute
                readyList.resolveWith( document, [ ReadyObj ] );

                // Trigger any bound ready events
                //if ( ReadyObj.fn.trigger ) {
                //    ReadyObj( document ).trigger( "ready" ).unbind( "ready" );
                //}
            }
        },
        bindReady: function() {
            if ( readyList ) {
                return;
            }
            readyList = ReadyObj._Deferred();

            // Catch cases where $(document).ready() is called after the
            // browser event has already occurred.
            if ( document.readyState === "complete" ) {
                // Handle it asynchronously to allow scripts the opportunity to delay ready
                return setTimeout( ReadyObj.ready, 1 );
            }

            // Mozilla, Opera and webkit nightlies currently support this event
            if ( document.addEventListener ) {
                // Use the handy event callback
                document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );
                // A fallback to window.onload, that will always work
                window.addEventListener( "load", ReadyObj.ready, false );

            // If IE event model is used
            } else if ( document.attachEvent ) {
                // ensure firing before onload,
                // maybe late but safe also for iframes
                document.attachEvent( "onreadystatechange", DOMContentLoaded );

                // A fallback to window.onload, that will always work
                window.attachEvent( "onload", ReadyObj.ready );

                // If IE and not a frame
                // continually check to see if the document is ready
                var toplevel = false;

                try {
                    toplevel = window.frameElement == null;
                } catch(e) {}

                if ( document.documentElement.doScroll && toplevel ) {
                    doScrollCheck();
                }
            }
        },
        _Deferred: function() {
            var // callbacks list
                callbacks = [],
                // stored [ context , args ]
                fired,
                // to avoid firing when already doing so
                firing,
                // flag to know if the deferred has been cancelled
                cancelled,
                // the deferred itself
                deferred  = {

                    // done( f1, f2, ...)
                    done: function() {
                        if ( !cancelled ) {
                            var args = arguments,
                                i,
                                length,
                                elem,
                                type,
                                _fired;
                            if ( fired ) {
                                _fired = fired;
                                fired = 0;
                            }
                            for ( i = 0, length = args.length; i < length; i++ ) {
                                elem = args[ i ];
                                type = ReadyObj.type( elem );
                                if ( type === "array" ) {
                                    deferred.done.apply( deferred, elem );
                                } else if ( type === "function" ) {
                                    callbacks.push( elem );
                                }
                            }
                            if ( _fired ) {
                                deferred.resolveWith( _fired[ 0 ], _fired[ 1 ] );
                            }
                        }
                        return this;
                    },

                    // resolve with given context and args
                    resolveWith: function( context, args ) {
                        if ( !cancelled && !fired && !firing ) {
                            // make sure args are available (#8421)
                            args = args || [];
                            firing = 1;
                            try {
                                while( callbacks[ 0 ] ) {
                                    callbacks.shift().apply( context, args );//shifts a callback, and applies it to document
                                }
                            }
                            finally {
                                fired = [ context, args ];
                                firing = 0;
                            }
                        }
                        return this;
                    },

                    // resolve with this as context and given arguments
                    resolve: function() {
                        deferred.resolveWith( this, arguments );
                        return this;
                    },

                    // Has this deferred been resolved?
                    isResolved: function() {
                        return !!( firing || fired );
                    },

                    // Cancel
                    cancel: function() {
                        cancelled = 1;
                        callbacks = [];
                        return this;
                    }
                };

            return deferred;
        },
        type: function( obj ) {
            return obj == null ?
                String( obj ) :
                class2type[ Object.prototype.toString.call(obj) ] || "object";
        }
    }
    // The DOM ready check for Internet Explorer
    function doScrollCheck() {
        if ( ReadyObj.isReady ) {
            return;
        }

        try {
            // If IE is used, use the trick by Diego Perini
            // http://javascript.nwbox.com/IEContentLoaded/
            document.documentElement.doScroll("left");
        } catch(e) {
            setTimeout( doScrollCheck, 1 );
            return;
        }

        // and execute any waiting functions
        ReadyObj.ready();
    }
    // Cleanup functions for the document ready method
    if ( document.addEventListener ) {
        DOMContentLoaded = function() {
            document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
            ReadyObj.ready();
        };

    } else if ( document.attachEvent ) {
        DOMContentLoaded = function() {
            // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
            if ( document.readyState === "complete" ) {
                document.detachEvent( "onreadystatechange", DOMContentLoaded );
                ReadyObj.ready();
            }
        };
    }
    function ready( fn ) {
        // Attach the listeners
        ReadyObj.bindReady();

        var type = ReadyObj.type( fn );

        // Add the callback
        readyList.done( fn );//readyList is result of _Deferred()
    }
    return ready;
})();
// PREPARAR DOCUMENT READY
var ready = (function(){

  var readyList,
      DOMContentLoaded,
      class2type = {};
      class2type["[object Boolean]"] = "boolean";
      class2type["[object Number]"] = "number";
      class2type["[object String]"] = "string";
      class2type["[object Function]"] = "function";
      class2type["[object Array]"] = "array";
      class2type["[object Date]"] = "date";
      class2type["[object RegExp]"] = "regexp";
      class2type["[object Object]"] = "object";

  var ReadyObj = {
      // Is the DOM ready to be used? Set to true once it occurs.
      isReady: false,
      // A counter to track how many items to wait for before
      // the ready event fires. See #6781
      readyWait: 1,
      // Hold (or release) the ready event
      holdReady: function( hold ) {
          if ( hold ) {
              ReadyObj.readyWait++;
          } else {
              ReadyObj.ready( true );
          }
      },
      // Handle when the DOM is ready
      ready: function( wait ) {
          // Either a released hold or an DOMready/load event and not yet ready
          if ( (wait === true && !--ReadyObj.readyWait) || (wait !== true && !ReadyObj.isReady) ) {
              // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
              if ( !document.body ) {
                  return setTimeout( ReadyObj.ready, 1 );
              }

              // Remember that the DOM is ready
              ReadyObj.isReady = true;
              // If a normal DOM Ready event fired, decrement, and wait if need be
              if ( wait !== true && --ReadyObj.readyWait > 0 ) {
                  return;
              }
              // If there are functions bound, to execute
              readyList.resolveWith( document, [ ReadyObj ] );

              // Trigger any bound ready events
              //if ( ReadyObj.fn.trigger ) {
              //    ReadyObj( document ).trigger( "ready" ).unbind( "ready" );
              //}
          }
      },
      bindReady: function() {
          if ( readyList ) {
              return;
          }
          readyList = ReadyObj._Deferred();

          // Catch cases where $(document).ready() is called after the
          // browser event has already occurred.
          if ( document.readyState === "complete" ) {
              // Handle it asynchronously to allow scripts the opportunity to delay ready
              return setTimeout( ReadyObj.ready, 1 );
          }

          // Mozilla, Opera and webkit nightlies currently support this event
          if ( document.addEventListener ) {
              // Use the handy event callback
              document.addEventListener( "DOMContentLoaded", DOMContentLoaded, false );
              // A fallback to window.onload, that will always work
              window.addEventListener( "load", ReadyObj.ready, false );

          // If IE event model is used
          } else if ( document.attachEvent ) {
              // ensure firing before onload,
              // maybe late but safe also for iframes
              document.attachEvent( "onreadystatechange", DOMContentLoaded );

              // A fallback to window.onload, that will always work
              window.attachEvent( "onload", ReadyObj.ready );

              // If IE and not a frame
              // continually check to see if the document is ready
              var toplevel = false;

              try {
                  toplevel = window.frameElement == null;
              } catch(e) {}

              if ( document.documentElement.doScroll && toplevel ) {
                  doScrollCheck();
              }
          }
      },
      _Deferred: function() {
          var // callbacks list
              callbacks = [],
              // stored [ context , args ]
              fired,
              // to avoid firing when already doing so
              firing,
              // flag to know if the deferred has been cancelled
              cancelled,
              // the deferred itself
              deferred  = {

                  // done( f1, f2, ...)
                  done: function() {
                      if ( !cancelled ) {
                          var args = arguments,
                              i,
                              length,
                              elem,
                              type,
                              _fired;
                          if ( fired ) {
                              _fired = fired;
                              fired = 0;
                          }
                          for ( i = 0, length = args.length; i < length; i++ ) {
                              elem = args[ i ];
                              type = ReadyObj.type( elem );
                              if ( type === "array" ) {
                                  deferred.done.apply( deferred, elem );
                              } else if ( type === "function" ) {
                                  callbacks.push( elem );
                              }
                          }
                          if ( _fired ) {
                              deferred.resolveWith( _fired[ 0 ], _fired[ 1 ] );
                          }
                      }
                      return this;
                  },

                  // resolve with given context and args
                  resolveWith: function( context, args ) {
                      if ( !cancelled && !fired && !firing ) {
                          // make sure args are available (#8421)
                          args = args || [];
                          firing = 1;
                          try {
                              while( callbacks[ 0 ] ) {
                                  callbacks.shift().apply( context, args );//shifts a callback, and applies it to document
                              }
                          }
                          finally {
                              fired = [ context, args ];
                              firing = 0;
                          }
                      }
                      return this;
                  },

                  // resolve with this as context and given arguments
                  resolve: function() {
                      deferred.resolveWith( this, arguments );
                      return this;
                  },

                  // Has this deferred been resolved?
                  isResolved: function() {
                      return !!( firing || fired );
                  },

                  // Cancel
                  cancel: function() {
                      cancelled = 1;
                      callbacks = [];
                      return this;
                  }
              };

          return deferred;
      },
      type: function( obj ) {
          return obj == null ?
              String( obj ) :
              class2type[ Object.prototype.toString.call(obj) ] || "object";
      }
  }
  // The DOM ready check for Internet Explorer
  function doScrollCheck() {
      if ( ReadyObj.isReady ) {
          return;
      }

      try {
          // If IE is used, use the trick by Diego Perini
          // http://javascript.nwbox.com/IEContentLoaded/
          document.documentElement.doScroll("left");
      } catch(e) {
          setTimeout( doScrollCheck, 1 );
          return;
      }

      // and execute any waiting functions
      ReadyObj.ready();
  }
  // Cleanup functions for the document ready method
  if ( document.addEventListener ) {
      DOMContentLoaded = function() {
          document.removeEventListener( "DOMContentLoaded", DOMContentLoaded, false );
          ReadyObj.ready();
      };

  } else if ( document.attachEvent ) {
      DOMContentLoaded = function() {
          // Make sure body exists, at least, in case IE gets a little overzealous (ticket #5443).
          if ( document.readyState === "complete" ) {
              document.detachEvent( "onreadystatechange", DOMContentLoaded );
              ReadyObj.ready();
          }
      };
  }
  function ready( fn ) {
      // Attach the listeners
      ReadyObj.bindReady();

      var type = ReadyObj.type( fn );

      // Add the callback
      readyList.done( fn );//readyList is result of _Deferred()
  }
  return ready;
})();
// FIM DOCUMENT READY
ready(function(){
  eventFire(document.querySelector(".container-capsulas .intensos"), 'click');
  eventFire(document.querySelector(".container-capsulas .t-25"), 'click');
});
// window.onload = function(){
//   setTimeout(() => {
//     ids = ["86570", "96331", "96167", "88094"];
//     var desconto = "";
//     ids.forEach(id => {
//       desconto = "";
//       desconto = document.querySelectorAll("div[data-id='" + id + "']")[0].parentElement.querySelector(".desconto");
//       if(desconto != null || desconto != undefined){
//         document.getElementById(id).querySelector(".icons-new-desconto").innerHTML = '<span class="desconto">'+ desconto.innerHTML + '</span>';
//       }else{
//         document.getElementById(id).querySelector(".icons-new-desconto").innerHTML = '';
//       }
//     }); 
//   });
// };
// change is-checked class on buttons
var buttonGroups = document.querySelectorAll('.button-group');

for ( var i=0; i < buttonGroups.length; i++ ) {
  var buttonGroup = buttonGroups[i];
  var onButtonGroupClick = getOnButtonGroupClick( buttonGroup );
  buttonGroup.addEventListener( 'click', onButtonGroupClick );
}

function getOnButtonGroupClick( buttonGroup ) {
  return function( event ) {
    // check for only button clicks
    var isButton = event.target.classList.contains('button');
    if ( !isButton ) {
      return;
    }
    if(event.target.classList.contains('active')){
      event.target.classList.remove('active');
    }else{
        var checkedButton = buttonGroup.querySelector('.active');
        if(checkedButton != null){
          checkedButton.classList.remove('active');
        }
        event.target.classList.add('active');
      }
    }
}
// flatten object by concatting values
function concatValues( obj ) {
  var value = '';
  for ( var prop in obj ) {
    value += obj[ prop ];
  }
  return value;
}
  function opencapsulas(e){
    e.classList.toggle("active");
    var p = [];
    p = e.querySelectorAll("p");
    p.forEach(function(element){
      element.classList.toggle("active");
    });
    e.querySelector(".btn-info").classList.toggle("active");
  }
  var clubbonus = document.getElementById("clubbonus");
  clubbonus.addEventListener("mouseover", function(){
    var items = clubbonus.querySelectorAll(".club-item .club-item-bg");
    for (var i = 0; i < items.length; i++) {
      items[i].parentElement.style.opacity = .5;
    }
    event.target.parentElement.style.opacity = 1;
  });
  clubbonus.addEventListener("mouseleave", function(){
    var items = clubbonus.querySelectorAll(".club-item .club-item-bg");
    for (var i = 0; i < items.length; i++) {
      items[i].parentElement.style.opacity = 1;
    }
  });
// TRANSFORM NAV FULL
  window.onscroll = function() {
    var element = document.querySelector(".nav");
    var maquinas = document.querySelector(".maquinasnespresso").getBoundingClientRect().top;
    var distanceTop = element.getBoundingClientRect().top;
    var distanceBot = element.getBoundingClientRect().bottom;
    var i = 0;
    if(window.innerWidth >= 767){
      if(maquinas <= 0){
        element.style.position = "fixed";
        element.style.top = 0;
        element.style.bottom = "initial";
        subitem = element.querySelectorAll(".item");
        for (i = 0; i < subitem.length; i++) {
          subitem[i].style.flexGrow = "1";
          subitem[i].style.WebkitFlexGrow = "1";
        }
        // hidescroll();
      }else if(distanceTop >= 0){
        element.style.position = "absolute";
        element.style.top = "initial";
        element.style.bottom = 0;
        subitem = element.querySelectorAll(".item");
        for (i = 0; i < subitem.length; i++) {
          subitem[i].style.flexGrow = "0";
          subitem[i].style.WebkitFlexGrow = "0";
        }
      }
    }else{
      if(maquinas <= 0){
        element.style.position = "fixed";
        element.style.bottom = 0;
        element.style.top = "initial";
        subitem = element.querySelectorAll(".item");
        for (i = 0; i < subitem.length; i++) {
          subitem[i].style.flexGrow = "1";
          subitem[i].style.WebkitFlexGrow = "1";
        }
        // hidescroll();
      }else if(distanceTop >= 0){
        element.style.position = "absolute";
        element.style.bottom = 0;
        element.style.top = "initial";
        subitem = element.querySelectorAll(".item");
        for (i = 0; i < subitem.length; i++) {
          subitem[i].style.flexGrow = "1";
          subitem[i].style.WebkitFlexGrow = "0";
        }
      }
    }
    if(isInViewport(document.querySelector('.maquinasnespresso'))) {
      document.querySelector('.maquinas').classList.add("active");
    }else{
      document.querySelector('.maquinas').classList.remove("active");
    }
    if(isInViewport(document.querySelector('.capsulas'))) {
      document.querySelector('.cafes').classList.add("active");
    }else{
      document.querySelector('.cafes').classList.remove("active");
    }
    if(isInViewport(document.querySelector('.clubnespresso'))) {
      document.querySelector('.servicos').classList.add("active");
    }else{
      document.querySelector('.servicos').classList.remove("active");
    }
  };
  
  // SOBRE TROCAR FOTO
  var sobre = document.getElementById("sobre");
  var sobrebutton = sobre.querySelectorAll(".tns-controls button");
  var sobreactive = "";
  for ( var i=0; i < sobrebutton.length; i++ ) {
    sobrebutton[i].onclick = activeslide;
  }
  
  function activeslide(){
    setTimeout(() => {
    sobreactive = sobre.querySelector(".informacoes.tns-slide-active").classList;
      switch (true) {
        case sobreactive.contains("reciclagem"):
        sobre.querySelector(".bg").style.backgroundImage = "url(https://www.girafa.com.br/hs/mundo-nespresso/img/bg-sobre-capsulas-reciclaveis.jpg?v=2)";
          break;
        
        case sobreactive.contains("praticas"):
        sobre.querySelector(".bg").style.backgroundImage = "url(https://www.girafa.com.br/hs/mundo-nespresso/img/bg-sobre-sacos-cafe.jpg?v=2)";
          break;
        
        case sobreactive.contains("historia"):
        sobre.querySelector(".bg").style.backgroundImage = "url(https://www.girafa.com.br/hs/mundo-nespresso/img/bg-sobre-ourchoice.jpg?v=2)";
          break;
          
        default:
          break;
      }
      
    }, 500);
  }

  // SCROLL LINK INTERNO
  function scrollIt(destination, duration = 200, easing = 'linear', callback) {

    const easings = {
      linear(t) {
        return t;
      },
      easeInQuad(t) {
        return t * t;
      },
      easeOutQuad(t) {
        return t * (2 - t);
      },
      easeInOutQuad(t) {
        return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
      },
      easeInCubic(t) {
        return t * t * t;
      },
      easeOutCubic(t) {
        return (--t) * t * t + 1;
      },
      easeInOutCubic(t) {
        return t < 0.5 ? 4 * t * t * t : (t - 1) * (2 * t - 2) * (2 * t - 2) + 1;
      },
      easeInQuart(t) {
        return t * t * t * t;
      },
      easeOutQuart(t) {
        return 1 - (--t) * t * t * t;
      },
      easeInOutQuart(t) {
        return t < 0.5 ? 8 * t * t * t * t : 1 - 8 * (--t) * t * t * t;
      },
      easeInQuint(t) {
        return t * t * t * t * t;
      },
      easeOutQuint(t) {
        return 1 + (--t) * t * t * t * t;
      },
      easeInOutQuint(t) {
        return t < 0.5 ? 16 * t * t * t * t * t : 1 + 16 * (--t) * t * t * t * t;
      }
    };
  
    const start = window.pageYOffset;
    const startTime = 'now' in window.performance ? performance.now() : new Date().getTime();
  
    const documentHeight = Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);
    const windowHeight = window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight;
    const destinationOffset = typeof destination === 'number' ? destination : destination.offsetTop;
    const destinationOffsetToScroll = Math.round(documentHeight - destinationOffset < windowHeight ? documentHeight - windowHeight : destinationOffset);
  
    if ('requestAnimationFrame' in window === false) {
      window.scroll(0, destinationOffsetToScroll);
      if (callback) {
        callback();
      }
      return;
    }
  
    function scroll() {
      const now = 'now' in window.performance ? performance.now() : new Date().getTime();
      const time = Math.min(1, ((now - startTime) / duration));
      const timeFunction = easings[easing](time);
      window.scroll(0, Math.ceil((timeFunction * (destinationOffsetToScroll - start)) + start));
  
      if (window.pageYOffset === destinationOffsetToScroll) {
        if (callback) {
          callback();
        }
        return;
      }
  
      requestAnimationFrame(scroll);
    }
  
    scroll();
  }

  document.querySelector('.maquinas').addEventListener('click', () => {
    scrollIt(
      document.querySelector('.maquinasnespresso'),
      500,
      'easeOutQuad'
    );
  });
  document.querySelector('.cafes').addEventListener('click', () => {
    scrollIt(
      document.querySelector('.capsulas'),
      500,
      'easeOutQuad'
    );
  });
  document.querySelector('.servicos').addEventListener('click', () => {
    scrollIt(
      document.querySelector('.clubnespresso'),
      500,
      'easeOutQuad'
    );
  });

// REMOVE DUPLICATE
(function removeduplicated(){
  var maquina = document.querySelectorAll(".maquina");
  for(var m = 1; m < maquina.length; m++){
    var maquinascor = maquina[m].querySelectorAll(".cor");
    var actualcor = "";
      for (var i = 0; i < maquinascor.length; i++) {
        var maquinaactual = maquinascor[i].classList;
        maquinaactual = maquinaactual.toString().slice(4);
        if(actualcor == maquinaactual){
          maquinascor[i].classList.add("hidden");
        }
        actualcor = maquinaactual;
      }
    }
})();
//------------- VIEWPORT ELEMENT
var isInViewport = function (elem) {
  var bounding = elem.getBoundingClientRect();
  return (
      bounding.top >= 0 &&
      bounding.left >= 0 &&
      bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
  );
};

(function(){
  var p1, p2, p3, resultado = "";
  
  document.querySelector(".dots .dot-1").classList.add("active");
  document.querySelector(".p1 .sim").addEventListener('click', () => {
    p1 = "1";
    document.querySelector(".p1").classList.add("hidden");
    document.querySelector(".p2").classList.remove("hidden");
    document.querySelector(".p2").classList.remove("hidden");
    document.querySelector(".dots .dot-2").classList.add("active");
  });
  document.querySelector(".p1 .nao").addEventListener('click', () => {
    p1 = "2";
    document.querySelector(".p1").classList.add("hidden");
    document.querySelector(".p3").classList.remove("hidden");
    document.querySelector(".dots .dot-2").classList.add("active");
  });

  document.querySelector(".p2 .sim").addEventListener('click', () => {
    p2 = "1";
    document.querySelector(".p2").classList.add("hidden");
    document.querySelector(".dots .dot-2").classList.add("active");
    resultado = p1 + " " + p2;
    calcresultado(resultado);
    document.querySelector(".questionario .check-result").classList.remove("hidden");
    document.querySelector(".questionario .reload-result").classList.remove("hidden");
  });
  document.querySelector(".p2 .nao").addEventListener('click', () => {
    p2 = "2";
    document.querySelector(".p2").classList.add("hidden");
    document.querySelector(".dots .dot-2").classList.add("active");
    resultado = p1 + " " + p2;
    calcresultado(resultado);
    document.querySelector(".check-result").classList.remove("hidden");
    document.querySelector(".reload-result").classList.remove("hidden");
  });

  document.querySelector(".p3 .sim").addEventListener('click', () => {
    p3 = "1";
    document.querySelector(".p3").classList.add("hidden");
    document.querySelector(".dots .dot-2").classList.add("active");
    resultado = p1 + " " + p3;
    calcresultado(resultado);
    document.querySelector(".questionario .check-result").classList.remove("hidden");
    document.querySelector(".questionario .reload-result").classList.remove("hidden");
  });
  document.querySelector(".p3 .nao").addEventListener('click', () => {
    p3 = "2";
    document.querySelector(".p3").classList.add("hidden");
    document.querySelector(".dots .dot-2").classList.add("active");
    resultado = p1 + " " + p3;
    calcresultado(resultado);
    document.querySelector(".questionario .check-result").classList.remove("hidden");
    document.querySelector(".questionario .reload-result").classList.remove("hidden");
  });
  document.querySelector(".reload-result").addEventListener('click', () => {
    perguntas = document.querySelectorAll(".pergunta");
    perguntas.forEach(function(pergunta){
      pergunta.classList.add("hidden");
    });
    document.querySelector(".p1").classList.remove("hidden");
    document.querySelector(".check-result").classList.add("hidden");
    document.querySelector(".reload-result").classList.add("hidden");
    p1, p2, p3, resultado = "";
    maquinas = document.querySelectorAll(".resultado .maquina");
    maquinas.forEach(function(maquina){
      maquina.classList.add("hidden");
    });
    
    dots = document.querySelectorAll(".dots .dot");
    dots.forEach(function(dot){
      dot.classList.remove("active");
    });
    document.querySelector(".dots .dot-1").classList.add("active");
    document.querySelector(".resultado").classList.remove("active");
  });
})();

function calcresultado(escolha){
  scrollIt(
    document.querySelector('.resultado'),
    500,
    'easeOutQuad'
  );
  document.querySelector(".resultado").classList.add("active");
  var maquinasresultado = document.querySelectorAll(".resultado .maquina");
  console.log(maquinasresultado);
  switch (escolha) {
    case "1 1":
      maquinasresultado.forEach(function(mr){
        mr.classList.add("hidden");
      });
      document.querySelector(".resultado #maquina5").classList.remove("hidden");
      document.querySelector(".resultado #maquina6").classList.remove("hidden");
      break;
    case "1 2":
      maquinasresultado.forEach(function(mr){
        mr.classList.add("hidden");
      });
      document.querySelector(".resultado #maquina2").classList.remove("hidden");
      document.querySelector(".resultado #maquina4").classList.remove("hidden");
      break;
    case "2 1":
      maquinasresultado.forEach(function(mr){
        mr.classList.add("hidden");
      });
      document.querySelector(".resultado #maquina1").classList.remove("hidden");
      document.querySelector(".resultado #maquina3").classList.remove("hidden");
      break;
    case "2 2":
      maquinasresultado.forEach(function(mr){
        mr.classList.add("hidden");
      });
      document.querySelector(".resultado #maquina7").classList.remove("hidden");
      break;
    default:
      break;
  }
}

//OPEN PICS
(function(){
  openpics = document.querySelectorAll(".morepics");
  openpics.forEach(function(openpic){
    openpic.addEventListener("click", function(){
      openpic.parentElement.querySelector(".outras").classList.toggle("active");
      openpic.classList.toggle("plus");
      openpic.classList.toggle("minus");
    });
  });
  fotos = document.querySelectorAll(".outras span");
  fotos.forEach(function(foto){
    if ("ontouchstart" in document.documentElement){
      foto.addEventListener("touchstart", function(){
        fotossec = foto.parentElement.querySelectorAll("span");
        fotossec.forEach(function(hoverfoto){
          hoverfoto.classList.remove("active");
        });
        // foto.parentElement.querySelector(".outras").classList.toggle("active");
        foto.classList.toggle("active");
        foto.parentElement.parentElement.querySelector("#fotoprincipal").src = foto.querySelector("img").src;
      });
    }
    else{
      foto.addEventListener("mouseover", function(){
        fotossec = foto.parentElement.querySelectorAll("span");
        fotossec.forEach(function(hoverfoto){
          hoverfoto.classList.remove("active");
        });
        // foto.parentElement.querySelector(".outras").classList.toggle("active");
        foto.classList.toggle("active");
        foto.parentElement.parentElement.querySelector("#fotoprincipal").src = foto.querySelector("img").src;
      });
    }
  });
  // var maquinas = document.querySelectorAll(".maquinasnespresso .maquina");
  // maquinas.forEach(maquina => {
  //   maquina.addEventListener("mouseover", function(){
  //     maquina.classList.remove("hover");
  //     maquina.classList.toggle("hover");
  //   });
  // });
})();
//CHANGE PICS COLOR
var data = [];
(function(){
  var request = new XMLHttpRequest();
  request.open('GET', 'https://www.girafa.com.br/hs/mundo-nespresso/js/maquinas-resultado.json?v=3', true);
  request.onload = function() {
    if (request.status >= 200 && request.status < 400) {
      // Success!
      data = JSON.parse(request.responseText);
    } else {
      // We reached our target server, but it returned an error

    }
  };

  request.onerror = function() {
    // There was a connection error of some sort
  };
  request.send();
})();
function changecolorpic(){
  cores = document.querySelectorAll(".cores");
  cores.forEach(function(coractive){
    coractive.childNodes[0].classList.add("on");
  });
  var colors = document.querySelectorAll(".cores .cor");
  colors.forEach(function(color){
      color.addEventListener("click", function(){
      if(!color.classList.contains("on")){
        var cor = color.classList.value.slice(4);
        var parent = color.parentElement.parentElement;
            // img = data.filter(function(item) {
            //     return item.opcoes[0].cor == cor;
            // })[0].opcoes.img1;
            function findWithAttr(array, attr, value) {
              for(var i = 0; i < array.length; i += 1) {
                  if(array[i][attr] === value) {
                      return array[i];
                  }
              }
              return -1;
          }
          var img = findWithAttr(data, 'id', parent.dataset.id);
          img = findWithAttr(img.opcoes, 'cor', cor);
            // var img = data.map(item => {
            //   item.opcoes = item.opcoes.filter(linha => {
            //     return linha.cor == cor;
            //   });
            //   return item.id == parent.dataset.id;
            // });
            parent.querySelector("#fotoprincipal").src = img.img1;
            parent.querySelector("#other1").src = img.img1;
            parent.querySelector("#other2").src = img.img2;
            parent.querySelector("#other3").src = img.img3;
            parent.querySelector(".btn-nes").href = img.link;
            parent.querySelectorAll(".cor").forEach(function(coractive){
              coractive.classList.remove("on");
            });
            color.classList.add("on");
        };
    });
  });
}
changecolorpic();
//------------- HIDE SCROLL
// function hidescroll(){
//   var ishiddenclass = 'hide',
//   b = (window.pageYOffset || document.documentElement.scrollTop)  - (document.documentElement.clientTop || 0);

//   var ms = 0;

//   document.onscroll = function(){
//     var t = (window.pageYOffset || document.documentElement.scrollTop)  - (document.documentElement.clientTop || 0),
//       a = t;

//     var elements = document.querySelectorAll('.nav');

//     elements.forEach(function(el) {
//       if(a < b) {
//         if (el.classList)
//           el.classList.remove(ishiddenclass);
//         else
//           el.className = el.className.replace(new RegExp('(^|\\b)' + ishiddenclass.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
//       }
//       if(a > b) {
//         if (el.classList)
//           el.classList.add(ishiddenclass);
//         else
//           el.className += ' ' + ishiddenclass;
//       }
//     });

//     b = a;
//   }; 
// }